/**
 * Created by lin on 2017/3/10.
 */
import fetch from 'isomorphic-fetch';
import Vue from 'vue';


let baseReq = {
  mode: 'cors',
  headers: {"Content-Type": "application/json"}
};
let baseUrl = '';
let userToken = '';
export function fetchPost(url, data, callback, auth = false, withCredentials = false, get,token) {
  debugger
  if(auth){
    userToken =token;
    baseUrl = 'https://www.qiyuanheshan.com/xnxw';
  }else{
    baseUrl = 'https://www.qiyuanheshan.com/xnxw-admin';
  }
  return fetchByBase(url, data, callback, auth, withCredentials, get);
}

export function fetchByBase(url, data, callback, auth = false, withCredentials = false, get) {
  debugger
  let token;

  if (withCredentials) {
    baseReq.credentials = 'include';
  }
  if (Vue.cookie.get('token')) {
    token= Vue.cookie.get('token');
  }
  if (auth||token) {
    baseReq.headers={
        "Content-Type": "application/json",
        "X-Xnxw-Token":userToken||token
    }
  }
  var xhr;
  if (get) {
    xhr = fetch(`${baseUrl}/${url}`, {
      ...baseReq,
      method: 'get',
    });
  } else {
    xhr = fetch(`${baseUrl}/${url}`, {
      ...baseReq,
      method: 'POST',
      body: data !== null && data !== undefined && data.toString() == '[object FormData]' ? data : data ? JSON.stringify(data) : null
    });
  }
  if (!callback) {
    return xhr.then(response => {
      return response.json();
    })
  } else {
    xhr.then(response => {
      return response.json();
    }).then(res => {
      callback(res)
    });
  }
}



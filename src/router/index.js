import Vue from 'vue'
import Router from 'vue-router'
import AboutUs from '@/components/AboutUs'
import Learning from '@/components/Learning'
import Rule from '@/components/Rule'
import Canvas from '@/components/Canvas'
import Login from '@/components/Login'
import VideoList from '@/components/VideoList'
import IndForList from '@/components/IndForList'
import ReleaseList from '@/components/ReleaseList'
import ForbidList from '@/components/ForbidList'
import Index from '@/components/Index'
import LuckDraw from '@/components/luckDraw'
import CreateStore from '@/components/createStore'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/videoList',
      name: 'VideoList',
      component: VideoList,
      meta: {
        title: '音频审核'
      }
    },{
      path: '/indForList',
      name: 'IndForList',
      component: IndForList,
      meta: {
        title: '认证审核'
      }
    },{
      path: '/forbidList',
      name: 'ForbidList',
      component: ForbidList,
      meta: {
        title: '封号列表'
      }
    },{
      path: '/aboutUs',
      name: 'AboutUs',
      component: AboutUs,
      meta: {
        title: '关于我们'
      }
    },{
      path: '/learning',
      name: 'Learning',
      component: Learning,
      meta: {
        title: '鲀说'
      }
    },
    {
      path: '/rule',
      name: 'Rule',
      component: Rule,
      meta: {
        title: '用户规则'
      }
    },
    {
      path: '/canvas',
      name: 'Canvas',
      component: Canvas,
      meta: {
        title: '生成海报'
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        title: '登录'
      }
    },  {
      path: '/releaseList',
      name: 'ReleaseList',
      component: ReleaseList,
      meta: {
        title: '投诉列表'
      }
    },
    {
      path: '/index',
      name: 'Index',
      component: Index,
      meta: {
        title: '鲀说'
      }
    },
    {
      path: '/draw',
      name: 'LuckDraw',
      component: LuckDraw,
      meta: {
        title: '鲀说'
      }
    },
    {
      path: '/createStore',
      name: 'CreateStore',
      component: CreateStore,
      meta: {
        title: '创建鲀铺'
      }
    }

  ]
})
